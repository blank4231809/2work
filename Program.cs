﻿using System.Collections.Generic;
using Patterns.Cafe;

namespace Patterns
{
    namespace Cafe
    {
        //composite
        abstract class AbstractMenuItem
        {
            public string Name { get; }
            protected AbstractMenuItem(string name) => Name = name;
        }
        class Menu : AbstractMenuItem
        {
            private Dictionary<string, AbstractMenuItem> Items { get; } = new();
            public Menu(string name)
                : base(name) { }

            public AbstractMenuItem? GetItem(string name) => Items.GetValueOrDefault(name);
            public void InsertItem(AbstractMenuItem item) => Items.Add(item.Name, item);
            public void RemoveItem(string name) => Items.Remove(name);
        }
        class MenuPosition : AbstractMenuItem
        {
            public decimal Price { get; }
            public MenuPosition(string name, decimal price)
                : base(name) => Price = price;
        }

        //builder
        class CafeOrder
        {
            public List<MenuPosition> Items { get; }
            public CafeOrder(params MenuPosition[] items) => Items = new(items);
            public string Description()
            {
                return Items.Aggregate(string.Empty, (last, item) => last + $"[{item?.Name ?? "null"}]");
            }
            public decimal Price()
            {
                return Items.Aggregate(0M, (last, item) => last + item.Price);
            }
        }

        interface IOrderBuilder
        {
            public IOrderBuilder WithMenu(Menu menu);
            public IOrderBuilder WithPositions(params string[] positions);
            public CafeOrder Create();
        }
        class ComplexDinnerBuilder : IOrderBuilder
        {
            public Menu? Menu { get; private set; } = null;

            public MenuPosition? Soup { get; private set; } = null;
            public MenuPosition? Salad { get; private set; } = null;
            public MenuPosition? Garnish { get; private set; } = null;
            public MenuPosition? Meat { get; private set; } = null;

            private MenuPosition? GetItemFromSubmenu(string menu, string item)
            {
                var submenu = (Menu?)Menu?.GetItem(menu);
                return (MenuPosition?)submenu?.GetItem(item);
            }
            public ComplexDinnerBuilder WithSoup(string name)
            {
                var item = GetItemFromSubmenu("soups", name);
                if (item is not null)
                    Soup = item;

                return this;
            }
            public ComplexDinnerBuilder WithSalad(string name)
            {
                var item = GetItemFromSubmenu("salads", name);
                if (item is not null)
                    Salad = item;

                return this;
            }
            public ComplexDinnerBuilder WithGarnish(string name)
            {
                var item = GetItemFromSubmenu("garnishes", name);
                if (item is not null)
                    Garnish = item;

                return this;
            }
            public ComplexDinnerBuilder WithMeat(string name)
            {
                var item = GetItemFromSubmenu("meat", name);
                if (item is not null)
                    Meat = item;

                return this;
            }

            public IOrderBuilder WithMenu(Menu menu)
            {
                Menu = menu;
                return this;
            }
            public IOrderBuilder WithPositions(params string[] menu)
            {
                return WithSoup(menu[0]).WithSalad(menu[1]).WithGarnish(menu[2]).WithMeat(menu[3]);
            }
            CafeOrder IOrderBuilder.Create()
            {
                return new CafeOrder(Soup, Salad, Garnish, Meat);
            }
        }
        class BreakfastBuilder : IOrderBuilder
        {
            public Menu? Menu { get; private set; } = null;
            public MenuPosition? Snack { get; private set; } = null;
            public MenuPosition? Drink { get; private set; } = null;

            private MenuPosition? GetItemFromSubmenu(string menu, string item)
            {
                var submenu = (Menu?)Menu?.GetItem(menu);
                return (MenuPosition?)submenu?.GetItem(item);
            }
            public BreakfastBuilder WithSnack(string name)
            {
                var item = GetItemFromSubmenu("snacks", name);
                if (item is not null)
                    Snack = item;

                return this;
            }
            public BreakfastBuilder WithDrink(string name)
            {
                var item = GetItemFromSubmenu("drinks", name);
                if (item is not null)
                    Drink = item;

                return this;
            }

            public IOrderBuilder WithMenu(Menu menu)
            {
                Menu = menu;
                return this;
            }
            public IOrderBuilder WithPositions(params string[] menu)
            {
                return WithSnack(menu[0]).WithDrink(menu[1]);
            }
            CafeOrder IOrderBuilder.Create()
            {
                return new CafeOrder(Snack, Drink);
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            var menu = new Menu("main menu");
            var meat = new Menu("meat");
            var soups = new Menu("soups");
            var salads = new Menu("salads");
            var garnishes = new Menu("garnishes");
            var snacks = new Menu("snacks");
            var drinks = new Menu("drinks");

            soups.InsertItem(new MenuPosition("Bear soup", 15));
            soups.InsertItem(new MenuPosition("Borsch", 25));
            salads.InsertItem(new MenuPosition("Caesar", 5));
            salads.InsertItem(new MenuPosition("Vegetable", 5));
            garnishes.InsertItem(new MenuPosition("Rise", 4));
            garnishes.InsertItem(new MenuPosition("Potato", 6));
            meat.InsertItem(new MenuPosition("Beef", 9));
            meat.InsertItem(new MenuPosition("Pork", 7));
            snacks.InsertItem(new MenuPosition("Fried eggs", 5));
            snacks.InsertItem(new MenuPosition("Toast", 3));
            drinks.InsertItem(new MenuPosition("Orange juice", 3));
            drinks.InsertItem(new MenuPosition("Green tea", 3));

            menu.InsertItem(meat);
            menu.InsertItem(soups);
            menu.InsertItem(salads);
            menu.InsertItem(garnishes);
            menu.InsertItem(snacks);
            menu.InsertItem(drinks);

            var dinner = new ComplexDinnerBuilder().WithMenu(menu).WithPositions("Borsch", "Vegetable", "Rise", "Beef").Create();
            Console.WriteLine($"Total price: {dinner.Price()}$");
            Console.WriteLine($"Description: {dinner.Description()}");

            var breakfast = new BreakfastBuilder().WithMenu(menu).WithPositions("Fried eggs", "Green tea").Create();
            Console.WriteLine($"Total price: {breakfast.Price()}$");
            Console.WriteLine($"Description: {breakfast.Description()}");
        }
    }
}

